/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.co.hairylemon.opcuaclient;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opcfoundation.ua.transport.security.SecurityMode;

/**
 *
 * @author dwm77
 */
public class OPCUAConnectionSettings {
    private Properties prop;
    public OPCUAConnectionSettings() {
        try {
            prop = new Properties();
            InputStream in = getClass().getResourceAsStream("/nz/co/hairylemon/opcuaclient/OPCUAConnection.properties");
            prop.load(in);
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(OPCUAConnectionSettings.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public String getUsername() {
        return prop.getProperty("username");
    } 
    public String getPassword() {
        return prop.getProperty("password");
    }
    public String getHostname() {
        return prop.getProperty("hostname");
    }
    public String getPort() {
        return prop.getProperty("port");
    }
    public SecurityMode getSecurityMode() {
        return SecurityMode.NONE;
    }
            
    public Locale getLocale() {
        return Locale.ENGLISH;
    }
    public String getApplicationName() {
        return prop.getProperty("application_name");
    }

    public String getServerNamespace() {
        return prop.getProperty("server_namespace");
    }

    public int getReconnectionSeconds() {
        return 3600;
    }
    
}
