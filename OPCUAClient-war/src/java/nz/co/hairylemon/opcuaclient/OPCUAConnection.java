/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.co.hairylemon.opcuaclient;

import com.prosysopc.ua.PkiFileBasedCertificateValidator;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.UserIdentity;
import com.prosysopc.ua.client.AddressSpaceException;
import com.prosysopc.ua.client.InvalidServerEndpointException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.nodes.UaDataType;
import com.prosysopc.ua.nodes.UaNode;
import com.prosysopc.ua.nodes.UaVariable;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opcfoundation.ua.builtintypes.DataValue;
import org.opcfoundation.ua.builtintypes.LocalizedText;
import org.opcfoundation.ua.builtintypes.NodeId;
import org.opcfoundation.ua.builtintypes.UnsignedInteger;
import org.opcfoundation.ua.common.NamespaceTable;
import org.opcfoundation.ua.core.ApplicationDescription;
import org.opcfoundation.ua.core.ApplicationType;
import org.opcfoundation.ua.core.Attributes;

/**
 *
 * @author dwm77
 */
public class OPCUAConnection {

    private static OPCUAConnection instance;
    private UaClient client;
    private OPCUAConnectionSettings settings;
    private Date connectionDate;
    private int reconnectionAfterSeconds;
    private String serverNamespace;

    private OPCUAConnection() {
        client = null;
        settings = new OPCUAConnectionSettings();
        serverNamespace = settings.getServerNamespace();
        reconnectionAfterSeconds = settings.getReconnectionSeconds();
        
    }

    public static OPCUAConnection getInstance() {
        if (instance == null) {
            instance = new OPCUAConnection();
        }
        return instance;
    }

    private void isConnected() {
        if (client == null || !client.isConnected()) {
            connect();
        }else{
            if (reconnectionNeeded()) {
                disconnect();
                connect();
            }
        }
    }

    private void connect() {
        try {
            client = new UaClient("opc.tcp://"+settings.getHostname()+":"+settings.getPort()+"");
            final PkiFileBasedCertificateValidator validator = new PkiFileBasedCertificateValidator();
            client.setCertificateValidator(validator);
            ApplicationDescription appDescription = new ApplicationDescription();
            appDescription.setApplicationName(new LocalizedText(settings.getApplicationName(), settings.getLocale()));
            appDescription.setApplicationType(ApplicationType.Client);
            client.setLocale(settings.getLocale());
            client.setTimeout(30000);
            client.setSecurityMode(settings.getSecurityMode());
            client.setUserIdentity(new UserIdentity());
            client.connect();
            connectionDate = new Date();
        } catch (ServiceException ex) {
            Logger.getLogger(OPCUAConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidServerEndpointException ex) {
            Logger.getLogger(OPCUAConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(OPCUAConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Object readValue(String tagName) {
        isConnected();
        try {
            NamespaceTable nt = client.getNamespaceTable();
            int ns = nt.getIndex(serverNamespace);
            NodeId nodeId = new NodeId(ns, tagName);
            DataValue value = client.readValue(nodeId);
            return value.getValue().getValue();
        } catch (ServiceException ex) {
            Logger.getLogger(OPCUAConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (StatusException ex) {
            Logger.getLogger(OPCUAConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean writeValue(String tagName, String value) {
        isConnected();
        boolean status = false;
        try {
            NamespaceTable nt = client.getNamespaceTable();
            int ns = nt.getIndex(serverNamespace);
            NodeId nodeId = new NodeId(ns, tagName);

            UnsignedInteger attributeId = Attributes.Value;
            UaNode node = client.getAddressSpace().getNode(nodeId);
            System.out.println("Writing to node " + nodeId + " - " + node.getDisplayName().getText());

            // Find the DataType if setting Value - for other properties you must
            // find the correct data type yourself
            UaDataType dataType = null;
            if (attributeId.equals(Attributes.Value)
                    && (node instanceof UaVariable)) {
                UaVariable v = (UaVariable) node;
                // Initialize DataType node, if it is not initialized yet
                if (v.getDataType() == null) {
                    v.setDataType(client.getAddressSpace().getType(
                            v.getDataTypeId()));
                }
                dataType = (UaDataType) v.getDataType();
                System.out.println("DataType: " + dataType.getDisplayName().getText());
            }

            Object convertedValue = dataType != null ? client.getAddressSpace().getDataTypeConverter().parseVariant(value, dataType) : value;
            status = client.writeAttribute(nodeId, attributeId, convertedValue);
            if (status) {
                System.out.println("Syncronised write successful");
            } else {
                System.out.println("Asyncronised write successful");
            }
            return status;

        } catch (AddressSpaceException ex) {
            Logger.getLogger(OPCUAConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceException ex) {
            Logger.getLogger(OPCUAConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (StatusException ex) {
            Logger.getLogger(OPCUAConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }

    private boolean reconnectionNeeded() {
        Calendar reconnectDate = Calendar.getInstance();
        reconnectDate.setTime(connectionDate);
        reconnectDate.add(Calendar.SECOND, reconnectionAfterSeconds);
        if (reconnectDate.before(new Date())) {
            return true;
        } else {
            return false;
        }
    }

    private void disconnect() {
        client.disconnect();
    }
}
